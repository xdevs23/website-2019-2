module.exports = {
  assetsDir: undefined,
  publicPath: undefined,
  outputDir: undefined,
  runtimeCompiler: undefined,
  productionSourceMap: false,
  parallel: undefined,
  css: undefined,

  pwa: {
    name: 'superboringdev',
    themeColor: '#304FFE',
    msTileColor: '#304FFE'
  }
}
